require 'spec_helper'

describe UsersController do

  let(:user) {FactoryGirl.create(:user)}

  before { user.admin! }
  before { sign_in user }


  describe 'PUT #update' do


    it "assigns user" do
      put :update,  id: user.id, user: {role: 'admin'}
      expect(assigns(:user)).to eq(user)
    end
  end

  describe 'PUT #destroy' do


    it "assigns user" do
      put :destroy, id: user.id, user: {role: 'admin'}
      expect(assigns(:user)).to eq(user)
    end
  end

  describe 'GET #show' do

    it "assigns user" do
      get :show, id: user.id, user: {role: 'admin'}
      expect(assigns(:user)).to eq(user)
    end
  end

end

