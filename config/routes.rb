Rails.application.routes.draw do
  resources :products
  resources :coupons
  devise_for :users, :controllers => { :registrations => 'registrations' }
  resources :users
  root :to => 'visitors#index'
end
