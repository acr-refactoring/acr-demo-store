class PaymentJob < ActiveJob::Base

  def perform(user)
    user.charge_card
  end

end
