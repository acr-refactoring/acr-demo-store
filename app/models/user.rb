class User < ActiveRecord::Base
  enum role: [:user, :vip, :admin]
  after_initialize :set_default_role, :if => :new_record?
  after_validation :set_coupon

  belongs_to :coupon
  accepts_nested_attributes_for :coupon
  validates_associated :coupon

  def set_default_role
    self.role ||= :user
  end

  def name
    "Application User"
  end

  def set_coupon
    return false if errors.any?
    return false if self.coupon.nil?
    coupon = Coupon.find_by code: self.coupon.code
    self.coupon = coupon
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def charge_card
    if !self.stripe_token.present?
      self.errors[:base] << 'Could not verify card.'
      raise ActiveRecord::RecordInvalid.new(self)
    end
     customer = Stripe::Customer.create(
      :email => self.email,
      :card => self.stripe_token
    )

    price = self.coupon.nil? ?
      Product.last.price : self.coupon.price
    title = Product.last.name
    charge = Stripe::Charge.create(
      :customer    => customer.id,
      :amount      => "#{price}",
      :description => "#{title}",
      :currency    => 'usd'
    )
    self.stripe_token = nil
    Rails.logger.info("Stripe transaction for #{self.email}") if charge[:paid] == true
  rescue Stripe::InvalidRequestError => e
    self.errors[:base] << e.message
    PaymentFailureMailer.failed_payment_email(self).deliver_now
    self.destroy
  rescue Stripe::CardError => e
    self.errors[:base] << e.message
    PaymentFailureMailer.failed_payment_email(self).deliver_now
    self.destroy
  end
end
