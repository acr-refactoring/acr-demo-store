class CreateProductService
  def call
    p1 = Product.where(name: 'Book One').first_or_initialize do |p|
      p.price = 20.00
      p.status = "Available"
    end
    p1.save!(:validate => false) if p1.new_record?

    p2 = Product.where(name: 'Book Two').first_or_initialize do |p|
      p.price = 40.00
      p.status = "Available"
    end
    p2.save!(:validate => false) if p2.new_record?
    p3 = Product.where(name: 'Book Three').first_or_initialize do |p|
      p.price = 60.00
      p.status = "Available"
    end
    p2.save!(:validate => false) if p2.new_record?
  end
end